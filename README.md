# README #

Code for joint pilot allocation and transmission power optimisation in massive MIMO systems using compatible sets. This code was used in the following paper

Emma Fitzgerald, Michał Pióro, Fredrik Tufvesson, "Massive MIMO Optimization With Compatible Sets", Wireless Communications IEEE Transactions on, vol. 18, no. 5, pp. 2794-2812, 2019. https://ieeexplore.ieee.org/document/8682237

Details about the optimisation models and the algorithms used to solve them can be found in the paper.

## Configuration and Installation ##

The optimisation models are implemented in AMPL, and are configured to use the CPLEX optimisation solver. Both AMPL and CPLEX should therefore be installed before running any of the models. Other optimisation solvers can also be used, with appropriate modifications in the scripts.

Individual models can be run with the provided shell scripts or AMPL run scripts. To generate scenarios and run the entire optimisation, Python should be installed. To plot the results, the matplotlib library for Python, gnuplot, and the epstopdf tool should be installed.

### Dependencies ###

AMPL
CPLEX
Python (2.7)
numpy
matplotlib
gnuplot
epstopdf

## Classes and Files ##

In the src directory can be found the optimisation models (.mod files) for the master problem, dual problem, and pricing problems. The scenario.mod file contains parameters and sets that are related to the scenario to be solved and are used in the other models. For some models, shell scripts (.sh files) are provided to more easily run the models on their own. For all models, AMPL run scripts (.run files) are provided.

There are also two Python scripts located in the src directory:

scenario.py: Contains classes for scenarios and c-sets
run_scenario.py: Main script to generate a set of scenarios and solve the optimisation problems for them

The plot_data.gpi file is a Gnuplot script that plots the results and places the output figures in the figures directory.

The src directory also contains subdirectories to store the input data, intermediate results, and solutions for each power control and precoding configuration. The precoding schemes are maximum ratio combining (MRC) or zero forcing (ZF) and the power control schemes are optimal, fair, static, and downlink (see the paper for details).

Finally, the scenarios directory is where the generated scenarios are placed when running run_scenarios.py.

## How to cite this repository ##

The following BibTeX entry can be used to cite this repository.

```
@misc{network_generator,
    howpublished = {\url{https://bitbucket.org/EIT_networking/massive_mimo_c-sets}},
    title = {Massive MIMO C-set Optimisation},
    author = {Emma Fitzgerald}
}
```

Work based on this code should also cite the accompanying paper.
```
@article{fitzgerald2019massive,
    title={Massive {MIMO} Optimization With Compatible Sets},
    author={Fitzgerald, Emma and Pi{\'o}ro, Micha{\l} and Tufvesson, Fredrik},
    journal={{IEEE} Transactions on Wireless Communications},
    volume={18},
    number={5},
    pages={2794--2812},
    year={2019},
    publisher={{IEEE}}
}

```

## Contact ##

The repository is owned and run by Emma Fitzgerald: emma.fitzgerald@eit.lth.se
