# Model for the dual problem 
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

var pi_uplink{nodes} >= 0;
var pi_downlink{nodes} >= 0;

maximize dual_objective:
    sum{k in nodes} (pi_uplink[k] * demand_uplink[k] + pi_downlink[k] * demand_downlink[k]);

subject to convexity{c in c_sets}:
    sum{k in nodes_c_set_uplink[c]} pi_uplink[k] + sum{k in nodes_c_set_downlink[c]} pi_downlink[k] <= 1;
