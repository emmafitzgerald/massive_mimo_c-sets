# Model for zero forcing with static power control 
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

param phi := min {k in nodes} channel[k];

subject to uplink_power_values {k in nodes}:
    power_uplink[k] = phi / channel[k];

subject to downlink_power_values {k in nodes}:
    power_downlink[k] * SNR_downlink * channel[k] * ((1 / SNR_downlink) * sum{n in nodes} (1 / channel[n]) + 
	    sum{n in nodes} ((fading[n] - channel[n]) / channel[n])) = (1 + SNR_downlink * (fading[k] - channel[k]));
