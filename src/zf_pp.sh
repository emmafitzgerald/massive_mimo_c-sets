#!/bin/sh

# Shell script to run the zero forcing pricing problem 
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


ampl scenario.mod zf_pp.mod zf_fair_power_control.mod master_test.dat optimal_pi.dat zf_pp.run
