# Model for the master problem 
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Coherence blocks assigned to each c-set
var blocks_used{c_sets}, integer, >= 0;

minimize frame_length:
    sum{c in c_sets} blocks_used[c];

subject to uplink_demands {k in nodes}:
    sum{c in c_sets_node_uplink[k]} blocks_used[c] >= demand_uplink[k];

subject to downlink_demands {k in nodes}:
    sum{c in c_sets_node_downlink[k]} blocks_used[c] >= demand_downlink[k];
