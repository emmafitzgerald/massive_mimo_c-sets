#!/usr/bin/python

# Classes for scenarios, c-sets, and nodes
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

default_SNR_uplink = 10.0
default_SNR_downlink = 10.0
default_num_antennas = 100
default_SINR_thresh = 1.0
default_pilot_samples = 1
default_num_pilots = 12
default_pathloss_exponent = 3.7
default_reference_distance = 200        # metres

class Scenario:
    def __init__(self, name="", 
            SNR_uplink=default_SNR_uplink, SNR_downlink=default_SNR_downlink,
            num_antennas=default_num_antennas, SINR_thresh=default_SINR_thresh, 
            pilot_samples=default_pilot_samples, num_pilots=default_num_pilots,
            pathloss_exponent=default_pathloss_exponent, reference_distance=default_reference_distance):
        self.c_sets = []
        self.nodes = []
        self.name = name
        self.SNR_uplink = SNR_uplink
        self.SNR_downlink = SNR_downlink
        self.num_antennas = num_antennas
        self.SINR_thresh = SINR_thresh
        self.pilot_samples = pilot_samples
        self.num_pilots = num_pilots
        self.pathloss_exponent = pathloss_exponent
        self.reference_distance = reference_distance

        self.master_objective = -1
        self.master_time = -1
        self.pp_time = -1
        self.dual_time = -1

        self.demands = {}

    def add_c_set(self, c_set):
        c_set.id = len(self.c_sets)
        self.c_sets.append(c_set)

    def add_nodes(self, nodes):
        for node in nodes:
            if node.demand not in self.demands:
                self.demands[node.demand] = node.demand
        node.set_channel_metric(self.calculate_channel_metric(node.distance))
        self.nodes.extend(nodes)

    def set_blocks_used(self, blocks_used):
        for c_set in self.c_sets:
            if c_set.id in blocks_used:
                c_set.blocks_used = blocks_used[c_set.id]
            else:
                c_set.blocks_used = 0

    def calculate_channel_metric(self, distance, precoding = "MRC"):
        beta = (distance / self.reference_distance) ** (-1.0 * self.pathloss_exponent)
        if precoding == "MRC":
            metric = beta
        elif precoding == "ZF":
            gamma = (self.pilot_samples * self.SNR_uplink * beta * beta) / (1.0 + self.pilot_samples * 
                        SNR_uplink * beta)
            metric = beta - gamma
        else:
            metric = distance

        return metric

    def generate_master_data(self):
        data = ""

        data += "data;\n\n"

        # Nodes
        data += "set nodes := "
        for node in self.nodes:
            data += str(node.id) + " "
        data += ";\n\n"

        # Node demands
        data += "param: demand_uplink demand_downlink :=\n"
        for node in self.nodes:
            data += str(node.id) + "\t" + str(node.uplink_demand) + "\t" + str(node.downlink_demand) + "\n"
        data += ";\n\n"

        # Node distances
        data += "param:  distance :=\n"
        for node in self.nodes:
            data += "\t" + str(node.id) + "\t" + str(node.distance) + "\n"
        data += ";\n\n"

        # Parameters
        data += "param SNR_uplink := " + str(self.SNR_uplink) + ";\n"
        data += "param SNR_downlink := " + str(self.SNR_downlink) + ";\n"
        data += "param num_antennas := " + str(self.num_antennas) + ";\n"
        data += "param SINR_thresh := " + str(self.SINR_thresh) + ";\n"
        data += "param pilot_samples := " + str(self.pilot_samples) + ";\n"
        data += "param num_pilots := " + str(self.num_pilots) + ";\n"
        data += "param pathloss_exponent := " + str(self.pathloss_exponent) + ";\n"
        data += "param reference_distance := " + str(self.reference_distance) + ";\n"

        return data

    def generate_c_set_data(self):
        data = ""

        data += "data;\n\n"

        data += "set c_sets := "
        for c_set in self.c_sets:
            data += str(c_set.id) + " "
        data += ";\n\n"

        data += "# Row labels are c-sets, column labels are nodes\n"
        data += "param node_active_uplink :=\n"
        data += "\t:\t"
        for node in self.nodes:
            data += str(node.id) + "\t"
        data += ":=\n"
        for c_set in self.c_sets:
            data += "\t" + str(c_set.id)
            for node in self.nodes:
                if node.id in c_set.uplink_nodes and c_set.uplink_nodes[node.id] == 1:
                    data += "\t1"
                else:
                    data += "\t0"
            data += "\n"
        data += ";\n\n"

        data += "# Row labels are c-sets, column labels are nodes\n"
        data += "param node_active_downlink :=\n"
        data += "\t:\t"
        for node in self.nodes:
            data += str(node.id) + "\t"
        data += ":=\n"
        for c_set in self.c_sets:
            data += "\t" + str(c_set.id)
            for node in self.nodes:
                if node.id in c_set.downlink_nodes and c_set.downlink_nodes[node.id] == 1:
                    data += "\t1"
                else:
                    data += "\t0"
            data += "\n"
        data += ";\n\n"
        
        return data
    
    def generate_outputs(self):
        outputs = {}
        outputs["master_obj"] = self.master_objective
        outputs["!num_nodes"] = len(self.nodes)
        outputs["num_c-sets"] = len(self.c_sets)
        outputs["master_time"] = self.master_time
        outputs["dual_time"] = self.dual_time
        outputs["pp_time"] = self.pp_time
        outputs["total_time"] = self.master_time + self.pp_time + self.dual_time
        outputs["total_pp_time"] = self.pp_time + self.dual_time
        outputs["iterations"] = self.iterations
        outputs["!scenario_name"] = self.name
        outputs["!SINR_thresh"] = self.SINR_thresh

        total_power = 0
        for c_set in self.c_sets:
            total_power += c_set.power_used()
        outputs["total_power"] = total_power

        self.total_power = total_power

        node_power = {}
        for c_set in self.c_sets:
            c_set_power = c_set.power_used_per_node()
            for nid in c_set_power.keys():
                if nid not in node_power:
                    node_power[nid] = 0.0
                node_power[nid] += c_set_power[nid]

        max_power = 0.0
        for node in node_power.keys():
            if node_power[node] > max_power:
                max_power = node_power[node]
        outputs["max_node_power"] = max_power

        self.max_node_power = max_power

        return outputs

class C_set:
    def __init__(self, id=0):
        self.id = id
        self.uplink_nodes = {}
        self.downlink_nodes = {}

        # Dictionaries mapping node IDs of nodes in the c-set to their power control coefficient values
        self.uplink_power = {}
        self.downlink_power = {}

        self.blocks_used = 0

    # Params: 
    # nodes:    dict of node IDs mapped to 1
    def update_uplink_nodes(self, nodes):
        self.uplink_nodes.update(nodes)

    # Params:
    # nodes:    list of nodes to be added
    def add_uplink_nodes(self, nodes):
        node_dict = {}
        for node in nodes:
            node_dict[node.ID] = 1
        self.update_uplink_nodes(node_dict)

    def update_downlink_nodes(self, nodes):
        self.downlink_nodes.update(nodes)

    # Params:
    # nodes:    list of nodes to be added
    def add_downlink_nodes(self, nodes):
        node_dict = {}
        for node in nodes:
            node_dict[node.ID] = 1
        self.update_downlink_nodes(node_dict)

    def update_uplink_power(self, power):
        self.uplink_power.update(power)

    def update_downlink_power(self, power):
        self.downlink_power.update(power)

    def power_used(self):
        power = 0
        for nid in self.uplink_nodes:
            if nid in self.uplink_power.keys():
                power += self.uplink_power[nid] * self.blocks_used
        for nid in self.downlink_nodes:
            if nid in self.downlink_power.keys():
                power += self.downlink_power[nid] * self.blocks_used

        return power

    def power_used_per_node(self):
        node_power = {}
        for nid in self.uplink_nodes:
            if nid not in node_power:
                node_power[nid] = 0.0
            if nid in self.uplink_power.keys():
                node_power[nid] += self.uplink_power[nid] * self.blocks_used
        for nid in self.downlink_nodes:
            if nid not in node_power:
                node_power[nid] = 0.0
            if nid in self.downlink_power.keys():
                node_power[nid] += self.downlink_power[nid] * self.blocks_used

        return node_power

class Node:
    def __init__(self, id=0, uplink_demand=0, downlink_demand=0, distance=1.0):
        self.id = id
        self.uplink_demand = uplink_demand
        self.downlink_demand = downlink_demand
        self.demand = uplink_demand if uplink_demand > downlink_demand else downlink_demand
        self.distance = distance

    def set_channel_metric(self, metric):
        self.channel_metric = metric

    # Comparison operators for Nodes compare using channel metrics
    def __eq__(self, other):
        if(isinstance(other, Node)):
            return self.channel_metric == other.channel_metric
        else:
            return NotImplemented

    def __ne__(self, other):
        if(isinstance(other, Node)):
            return self.channel_metric != other.channel_metric
        else:
            return NotImplemented

    def __lt__(self, other):
        if(isinstance(other, Node)):
            return self.channel_metric < other.channel_metric
        else:
            return NotImplemented

    def __gt__(self, other):
        if(isinstance(other, Node)):
                return self.channel_metric > other.channel_metric
        else:
            return NotImplemented
