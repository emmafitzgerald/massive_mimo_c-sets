# Pricing problem for zero forcing 
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

param pi_uplink{nodes} >= 0, default 0;
param pi_downlink{nodes} >= 0, default 0;

param max_fading := max {k in nodes} fading[k];
param big_delta := SINR_thresh * (card{nodes} * SNR_uplink * max_fading + 1);

var u{nodes} >= 0, <= 1;
var u_uplink{nodes} binary;
var u_downlink{nodes} binary;
var power_uplink{nodes} >= 0;
var power_downlink{nodes} >= 0;
var x_uplink{nodes, nodes} >= 0;
var x_downlink{nodes, nodes} >= 0;
var L_uplink{nodes} >= 0;
var L_downlink{nodes} >= 0;

maximize pp_objective:
    sum{k in nodes} (u_uplink[k] * pi_uplink[k] + u_downlink[k] * pi_downlink[k]);
    
subject to uplink_SINR{k in nodes}:
   big_delta * (1 - u_uplink[k]) + SNR_uplink * channel[k] * (num_antennas * power_uplink[k] - L_uplink[k]) >=  
	SINR_thresh * (1 + SNR_uplink * sum{n in nodes} ((fading[n] - channel[n]) * x_uplink[n, n])); 

subject to define_L_uplink{n in nodes}:
    L_uplink[n] = sum{k in nodes} x_uplink[k, n];   # u_uplink[k] * power_uplink[n]

# Auxilliary variable constraints to eliminate variable multiplication
subject to auxilliary1_uplink{k in nodes, n in nodes}:
    x_uplink[k, n] <= u_uplink[k];

subject to auxilliary2_uplink{k in nodes, n in nodes}:
    x_uplink[k, n] <= power_uplink[n];

subject to auxilliary3_uplink{k in nodes, n in nodes}:
    x_uplink[k, n] >= power_uplink[n] + u_uplink[k] - 1 + (u_uplink[k] - 1) * big_delta;

subject to downlink_SINR{k in nodes}:
    big_delta * (1 - u_downlink[k]) + SNR_downlink * channel[k] * (num_antennas * power_downlink[k] - L_downlink[k]) >= 
	SINR_thresh * (1 + SNR_downlink * (fading[k] - channel[k]) * sum{n in nodes} (x_downlink[n, n]));

subject to define_L_downlink{n in nodes}:
    L_downlink[n] = sum{k in nodes} x_downlink[k, n];   # u_downlink[k] * power_downlink[n]

# Auxilliary variable constraints to eliminate variable multiplication
subject to auxilliary1_downlink{k in nodes, n in nodes}:
    x_downlink[k, n] <= u_downlink[k];

subject to auxilliary2_downlink{k in nodes, n in nodes}:
    x_downlink[k, n] <= power_downlink[n];

subject to auxilliary3_downlink{k in nodes, n in nodes}:
    x_downlink[k, n] >= power_downlink[n] + u_downlink[k] - 1;

subject to active_uplink{k in nodes}:
    u[k] >= u_uplink[k];

subject to active_downlink{k in nodes}:
    u[k] >= u_downlink[k];

subject to inactive{k in nodes}:
    u[k] <= u_uplink[k] + u_downlink[k];

subject to pilot_allocation:
    sum{k in nodes} u[k] <= num_pilots;
