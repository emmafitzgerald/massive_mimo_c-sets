# Sets and parameters common to all optimisation problems in a given scenario
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set c_sets;
set nodes;

# Whether or not a node is active for uplink in a given c-set
param node_active_uplink{c_sets, nodes} binary default 0;
# Whether or not a node is active for downlink in a given c-set
param node_active_downlink{c_sets, nodes} binary default 0;
# Whether or not a given node is active in a given c-set, x_k
param node_active_c_set{c in c_sets, k in nodes} binary := max(node_active_uplink[c, k], node_active_downlink[c, k]);

# Set of c-sets in which a given node is active (in any role)
set c_sets_node{k in nodes} = {c in c_sets : node_active_c_set[c, k] = 1};
# Set of c_sets in which a given node is active for uplink
set c_sets_node_uplink{k in nodes} = {c in c_sets : node_active_uplink[c, k] = 1};
# Set of c_sets in which a given node is active for downlink
set c_sets_node_downlink{k in nodes} = {c in c_sets : node_active_downlink[c, k] = 1};

# Set of nodes that are active in a given c-set
set nodes_c_set{c in c_sets} = {k in nodes : node_active_c_set[c, k] = 1};
# Set of nodes that are active on the uplink in a given c-set
set nodes_c_set_uplink{c in c_sets} = {k in nodes : node_active_uplink[c, k] = 1};
# Set of nodes that are active on the downlink in a given c-set
set nodes_c_set_downlink{c in c_sets} = {k in nodes : node_active_downlink[c, k] = 1};

# Demands
param demand_uplink{nodes} >= 0, default 0;
param demand_downlink{nodes} >= 0, default 0;

# Radio parameters
param SNR_uplink >= 0;
param SNR_downlink >= 0;
param num_antennas >= 0, integer;
param SINR_thresh >= 0;
param pathloss_exponent >= 0;
param reference_distance >= 0;
param distance{k in nodes} >= 0;
param fading{k in nodes} := (distance[k] / reference_distance) ** (-1.0 * pathloss_exponent);
param pilot_samples integer, >= 0;
param num_pilots integer, >= 0;
# Mean square channel estimate
param channel{k in nodes} := (pilot_samples * SNR_uplink * fading[k] * fading[k]) / (1 + pilot_samples * SNR_uplink * fading[k]);
#param channel{k in nodes} := fading[k];
