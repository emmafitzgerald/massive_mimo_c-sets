# Model for zero forcing with fair power control 
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

param big_gamma = max {k in nodes} channel[k];

var phi >= 0;
var z{nodes} binary;
var y_uplink{nodes} binary;
var y_downlink{nodes, nodes} >= 0;

subject to uplink_power_values {k in nodes}:
    power_uplink[k] = phi / channel[k];

subject to phi_upper {k in nodes}:
    phi <= channel[k] * u_uplink[k] + (1 - u_uplink[k]) * big_gamma;

subject to phi_lower:
    phi >= sum{k in nodes} (channel[k] * z[k]);

subject to aux1_up {k in nodes}:
    y_uplink[k] <= u_uplink[k];

subject to aux2_up {k in nodes}:
    y_uplink[k] <= z[k];

subject to aux3_up {k in nodes}:
    y_uplink[k] >= u_uplink[k] + z[k] - 1;

subject to select_one_y:
    sum{k in nodes} y_uplink[k] <= 1;

# At least one y[k] must be 1 as long as at least one node transmits
subject to none_transmit {k in nodes}:
    sum{n in nodes} y_uplink[n] >= u_uplink[k];

subject to downlink_power_values {k in nodes}:
    SNR_downlink * channel[k] * ((1 / SNR_downlink) * sum{n in nodes} (x_downlink[n,k] / channel[n]) + 
	    sum{n in nodes} (((fading[n] - channel[n]) * x_downlink[n,k]) / channel[n])) = (1 + SNR_downlink * (fading[k] - channel[k])) * u_downlink[k];
