#!/usr/bin/python

# Main Python script to generate scenarios and run the optimisation problems 
#
# (C) Copyright 2019 Emma Fitzgerald
# emma.fitzgerald@eit.lth.se
#
# This program is distributed under the terms of the GNU Lesser General Public License.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pickle
import os
from shutil import copyfile
import sys
import subprocess
import scenario
from scenario import Scenario, C_set, Node

# Class to describe a precoding and power control configuration
class Config:
    def __init__(self, name="", description="", pp_run_file="", directory=""):
        self.name = name
        self.description = description
        self.pp_run_file = pp_run_file
        self.directory = directory

# Get average and confidence interval delta of a vector of values
def process_vector(v):
    if len(v) == 0:
        print "Empty vector"
        return (-1, -1)
    avg = numpy.mean(v)
    stddev = numpy.std(v)
    n = len(v)
    delta = 1.96 * (stddev / math.sqrt(n)) 
    return (avg, delta)

# Process a vector of result vectors into a string for output with the average and confidence interval
# delta of each result vector
def process_results(results):
    result_string = "" 
    for key in sorted(results.keys()):
        avg, delta = process_vector(results[key])
        result_string += "\t" + str(avg) + "\t" + str(delta)
    return result_string

# Run AMPL with the provided run script, send output to the given output path
def run_ampl(run_file, output_path):
    p = subprocess.Popen(["time", "-f %U %S", "ampl", run_file],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outfile = open(output_path, "w") 
    for line in p.stdout:
        outfile.write(line)
    outfile.close()
    ttime = -1.0 
    for line in p.stderr:
        pieces = line.split()
        try: 
            ttime = float(pieces[0]) + float(pieces[1])
        except:
            print run_file
            print output_path
            print line 
            continue
    print "Solution time:", ttime

    return ttime

# input file dual.out
# output file optimal_pi.dat
# returns dual objective
def parse_dual_output():
    solution_file = open("dual.out", "r")

    uplink_pi = {}
    downlink_pi = {}
    objective = -1

    state = "START"
    for line in solution_file:
        if "objective" in line:
            objective = float(line.split()[-1])
            continue
        if line.startswith(";"):
            state = "START"
            continue
        if line.startswith("pi_uplink"):
            state = "UPLINK"
            continue
        if state == "UPLINK":
            pieces = line.split()
            if float(pieces[1]) > 0.0:
                uplink_pi[pieces[0]] = pieces[1]
        if line.startswith("pi_downlink"):
            state = "DOWNLINK"
            continue
        if state == "DOWNLINK":
            pieces = line.split()
            if float(pieces[1]) > 0.0:
                downlink_pi[pieces[0]] = pieces[1]
            continue
    
    solution_file.close()

    outfile = open("optimal_pi.dat", "w")
    outfile.write("data;\n\n")

    outfile.write("param: pi_uplink :=\n")
    for pi in uplink_pi.keys():
        outfile.write(pi + "\t" + uplink_pi[pi] + "\n")
    outfile.write(";\n\n")

    outfile.write("param: pi_downlink :=\n")
    for pi in downlink_pi.keys():
        outfile.write(pi + "\t" + downlink_pi[pi] + "\n")
    outfile.write(";\n\n")

    return objective

# input file pp.out
# returns pricing problem objective, new c-set (if any)
def parse_pp_output():
    solution_file = open("pp.out", "r")

    objective = -1
    uplink_nodes = {}
    downlink_nodes = {}
    uplink_power = {}
    downlink_power = {}

    state = "START"
    for line in solution_file:
        if "objective" in line:
            objective = float(line.split()[-1])
            continue
        if line.startswith(";"):
            state = "START"
            continue
        if line.startswith("u_uplink"):
            state = "UPLINK_NODES"
            continue
        if state == "UPLINK_NODES":
            pieces = line.split()
            if pieces[1] == "1":
                # Node is active on the uplink for the new c-set
                node_id = int(pieces[0])
                uplink_nodes[node_id] = 1
            continue
        if line.startswith("u_downlink"):
            state = "DOWNLINK_NODES"
            continue
        if state == "DOWNLINK_NODES":
            pieces = line.split()
            if pieces[1] == "1":
                # Node is active on the downlink for the new c-set
                node_id = int(pieces[0])
                downlink_nodes[node_id] = 1
            continue
        if line.startswith("power_uplink"):
            state = "UPLINK_POWER"
            continue
        if state == "UPLINK_POWER":
            pieces = line.split()
            node_id = int(pieces[0])
            uplink_power[node_id] = float(pieces[1])
            continue
        if line.startswith("power_downlink"):
            state = "DOWNLINK_POWER"
            continue
        if state == "DOWNLINK_POWER":
            pieces = line.split()
            node_id = int(pieces[0])
            downlink_power[node_id] = float(pieces[1])
            continue

    solution_file.close()

    c_set = C_set()
    c_set.update_uplink_nodes(uplink_nodes)
    c_set.update_downlink_nodes(downlink_nodes)
    c_set.update_uplink_power(uplink_power)
    c_set.update_downlink_power(downlink_power)

    return objective, c_set

# input file master.out
# returns master objective, dictionary of c-set ids to blocks used by each c-set
def parse_master_output():
    solution_file = open("master.out", "r")

    blocks_used = {}
    objective = -1

    state = "START"
    for line in solution_file:
        if "objective" in line:
            objective = int(line.split()[-1])
            continue
        if line.startswith(";"):
            state = "START"
            continue
        if line.startswith("blocks_used"):
            state = "BLOCKS"
            continue
        if state == "BLOCKS":
            pieces = line.split()
            blocks_used[int(pieces[0])] = int(pieces[1])
    
    solution_file.close()

    return objective, blocks_used

# Generate a set of scenarios for optimisation
def generate_systematic_scenarios():
    distances = [50.0, 500.0]   # Distances of nodes from the base station
    demands = [2, 10]   # Traffic demand volumes
    num_nodes = 40  # Total number of nodes
    num_near = 8    # Number of nodes close to the base station
    num_far = 32    # Number of nodes far from the base station

    # Generate combinations of the different parameters
    # Uncomment (and comment the following line) to run with a varying number of nodes
#    for num_nodes in [4, 8, 12, 16, 20, 24, 28, 32, 36]:
    for num_nodes in [40]:
        combinations = []
        combinations.append([])
        for i in xrange(num_near):
            combinations[0].append([1, 1, 0])
        for i in xrange(num_far):
            combinations[0].append([0, 0, 1])

        combinations.append([])
        for i in xrange(num_near):
            combinations[1].append([0, 0, 0])
        for i in xrange(num_far):
            combinations[1].append([1, 1, 1])    

        combinations.append([])
        for i in xrange(num_near):
            combinations[2].append([0, 1, 0])
        for i in xrange(num_far):
            combinations[2].append([1, 0, 1])    

        combinations.append([])
        for i in xrange(num_near):
            combinations[3].append([1, 0, 0])
        for i in xrange(num_far):
            combinations[3].append([0, 1, 1])    

        combinations.append([])
        for i in xrange(num_near):
            combinations[4].append([1, 0, 0])
        for i in xrange(num_far):
            combinations[4].append([1, 0, 1])    

        combinations.append([])
        for i in xrange(num_near):
            combinations[5].append([0, 1, 0])
        for i in xrange(num_far):
            combinations[5].append([0, 1, 1])    

        # Create a scenario for each parameter combination
        for i in xrange(len(combinations)):
            # Uncomment (and comment the following line) to run with varying SINR threshold values
#            for thresh in [50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200]:
            for thresh in [1]:
                name = "scenario_" + str(i) + "_" + str(thresh)
                scenario = Scenario(name = name, SINR_thresh = thresh)

                # Uncomment if varying the number of nodes
#                name = "scenario_" + str(i) + "_" + str(num_nodes)
#                scenario = Scenario(name = name)

                # Create nodes with the given distances and uplink and downlink demands
                nodes = []
                for n in xrange(num_nodes):
                    up, down, dist = combinations[i][n]
                    node = Node(n, demands[up], demands[down], distances[dist])
                    nodes.append(node)
                scenario.add_nodes(nodes)

                # Create initial c-sets, each with only one node and full transmission power
                for node in nodes:
                    c_set = C_set(node.id)
                    c_set.update_uplink_nodes({node.id : 1})
                    c_set.update_downlink_nodes({node.id : 1})
                    c_set.update_uplink_power({node.id : 1.0})
                    c_set.update_downlink_power({node.id : 1.0})
                    scenario.add_c_set(c_set)

                # Save the generated scenarios
                f = open("scenarios/" + name + ".scn" , "w")
                pickle.dump(scenario, f)
                f.close()

# Remove all files from previous optimisation runs in a given config directory
def clean_directory(config):
    os.chdir(config.directory)
    for directory in ["c_sets", "dual_out", "master_data", "master_out", "pp_data", "pp_out", "solutions"]:
        os.chdir(directory)
        os.system("rm -f ./*")
        os.chdir("..")
    os.chdir("..")

if __name__ == "__main__":

    generate_systematic_scenarios()

    # Create configurations with name, description, AMPL run file, and data directory
    configs = []
    configs.append(Config("mrc_fair", "Maximum Ratio Combining with fair power control", "mrc_pp_fair.run", "mrc_fair"))
    configs.append(Config("mrc_static", "Maximum Ratio Combining with static power control", "mrc_pp_static.run", "mrc_static"))
    configs.append(Config("mrc_optimal", "Maximum Ratio Combining with optimal power control", "mrc_pp_optimal.run", "mrc_optimal"))
    configs.append(Config("mrc_downlink", "Maximum Ratio Combining with no uplink power control", "mrc_pp_downlink.run", "mrc_downlink"))
    configs.append(Config("zf_fair", "Zero Forcing with fair power control", "zf_pp_fair.run", "zf_fair"))
    configs.append(Config("zf_static", "Zero Forcing with static power control", "zf_pp_static.run", "zf_static"))
    configs.append(Config("zf_optimal", "Zero Forcing with optimal power control", "zf_pp_optimal.run", "zf_optimal"))
    configs.append(Config("zf_downlink", "Zero Forcing with no uplink power control", "zf_pp_downlink.run", "zf_downlink"))

    outputs = {}
    for config in configs:
        # Remove data and results from previous runs
        clean_directory(config)

        # Prepare results file
        results_file = open(config.directory + "/results.dat", "w")
        results_header = False

        # Run each scenario
        for scenario_path in sorted(os.listdir("scenarios")):
            if not scenario_path.endswith(".scn"):
                continue

            # Read in scenario and generate master data
            scenario_file = open("scenarios/" + scenario_path, "r")
            scenario = pickle.load(scenario_file)
            scenario_file.close()
            master_data = scenario.generate_master_data()
        
            # Copy master data to master.dat
            master_file = open("master.dat", "w")
            master_file.write(master_data)
            master_file.close()
            
            # Copy master.dat to config dir
            copyfile("master.dat", config.directory + "/master_data/" + scenario.name + "_master.dat")

            # Copy initial c-sets to c_sets.dat
            c_set_data = scenario.generate_c_set_data()
            c_sets_file = open("c_sets.dat", "w")
            c_sets_file.write(c_set_data)
            c_sets_file.close()

            iterations = 1
            pp_time = 0.0
            dual_time = 0.0
            while(True):
                print
                print "Scenario: " + scenario.name + ", config: " + config.name
                print "Pricing problem iteration: " + str(iterations)
                sys.stdout.flush()

                # Run dual and write to dual_out dir
                time = run_ampl("dual.run", "dual.out")
                dual_time += time
                copyfile("dual.out", config.directory + "/dual_out/" + scenario.name + "_dual_" 
                        + str(iterations).zfill(3) + ".out")

                # Parse dual output, create optimal_pi.dat, copy to pp_data dir
                dual_objective = parse_dual_output()
                copyfile("optimal_pi.dat", config.directory + "/pp_data/" + scenario.name + "_optimal_pi_" 
                        + str(iterations).zfill(3) + ".dat")

                print "Solved dual, objective = ", dual_objective
                sys.stdout.flush()

                # Run pricing problem for config and copy output
                time = run_ampl(config.pp_run_file, "pp.out")
                pp_time += time
                copyfile("pp.out", config.directory + "/pp_out/" + scenario.name + "_pp_" 
                        + str(iterations).zfill(3) + ".out")

                # Parse PP output
                pp_objective, c_set = parse_pp_output()
                print "Solved pricing problem, objective = ", pp_objective
                sys.stdout.flush()
                
                # If new c-set generated
                eps = 0.000005
                if pp_objective > 1.0 + eps:
                    # Add c-set to list
                    scenario.add_c_set(c_set)

                    # Write new c_sets.dat
                    c_set_data = scenario.generate_c_set_data()
                    c_sets_file = open("c_sets.dat", "w")
                    c_sets_file.write(c_set_data)
                    c_sets_file.close()
                else:
                    # Copy c_sets.dat to data dir for config
                    copyfile("c_sets.dat", config.directory + "/c_sets/" + scenario.name + "_c_sets_final.dat")

                    # Stopping condition met, exit loop
                    print "Pricing problem solutino <= 1, pricing complete"
                    sys.stdout.flush()
                    break
                
                iterations += 1

            # Run master problem
            master_time = run_ampl("master.run", "master.out")
            copyfile("master.out", config.directory + "/master_out/" + scenario.name + "_master.out") 
            master_objective, blocks_used = parse_master_output()
            print "Solved master problem, objective = " + str(master_objective)
            sys.stdout.flush()

            # Record blocks used, time, total power uplink and downlink, max per-node power uplink
            scenario.set_blocks_used(blocks_used)
            scenario.master_objective = master_objective
            scenario.master_time = master_time
            scenario.pp_time = pp_time
            scenario.dual_time = dual_time
            scenario.iterations = iterations
            output_dict = scenario.generate_outputs()
            solution_file = open(config.directory + "/solutions/" + scenario.name + "_solution.p", "w")
            pickle.dump(scenario, solution_file)
            solution_file.close()

            # Write results
            for key in output_dict.keys():
                if key not in outputs:
                    outputs[key] = []
                outputs[key].append(output_dict[key])

            if not results_header:
                results_file.write("# ")
                column_num = 1
                for key in sorted(output_dict.keys()):
                    results_file.write(str(column_num) + " " + key + "\t")
                    column_num += 1
                results_file.write("\n")
                results_header = True

            for key in sorted(output_dict.keys()):
                results_file.write(str(output_dict[key]) + "\t")
            results_file.write("\n")
            results_file.flush()
            
        # Calculate and record statistics for config
        results_file.close()
